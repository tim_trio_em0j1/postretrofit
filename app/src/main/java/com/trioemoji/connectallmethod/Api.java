package com.trioemoji.connectallmethod;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;

/**
 * Created by Belal on 10/2/2017.
 */

public interface Api {

    String BASE_URL = "http://192.168.8.100/apisnapay/index.php/";
    @GET("kontak")
    Call<List<Nama>> getHeroes();
    @FormUrlEncoded
    @POST("kontak")
    Call<Nama> insertUser(
            @Field("nama") String nama,
            @Field("nomor") String nomor);
}
