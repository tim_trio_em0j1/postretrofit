package com.trioemoji.connectallmethod;

//import com.google.gson.annotations.SerializedName;
public class Nama{

    //	@SerializedName("nama")
    private String nama;
    private String id;
    private String nomor;

    public void setNama(String nama){
        this.nama = nama;
    }

    public String getNama(){
        return nama;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return id;
    }

    public void setNomor(String nomor){
        this.nomor = nomor;
    }

    public String getNomor(){
        return nomor;
    }

    @Override
    public String toString(){
        return
                "Nama{" +
                        "nama = '" + nama + '\'' +
                        ",id = '" + id + '\'' +
                        ",nomor = '" + nomor + '\'' +
                        "}";
    }
}
